# GraphEditor in Vue.js

## Summary

Take this vanilla Javascript / HTML app (https://github.com/jgraph/mxgraph/tree/master/javascript/examples/grapheditor) defined in this URL and create it in Vue.js using TypeScript and Bootstrap Vue.

### Requirements

1. Porting grapheditor to vuejs. Integrating into a vue clinet, and not wrapping.
    * GraphEditor shall run in vuejs natively. This UX shall be duplicated 100%. 
1. Take this vanilla Javascript / HTML app defined in this URL and create it in Vue.js using TypeScript and Bootstrap Vue:
    * https://github.com/jgraph/mxgraph/tree/master/javascript/examples/grapheditor
     * Required Package:
       * https://github.com/jgraph/mxgraph
       * Version `4.1.1` or `latest`
1. mxgraph 
    * As an npm `yarn add mxgraph@^latest` unless required not to be. Need to review prior to changing from an NPM.
1. UX is based on and shall use bootstrap-vue.
    * https://bootstrap-vue.org/
    * https://github.com/bootstrap-vue/bootstrap-vue
    * Version `2.12.0` or `latest`
1. Vue CLI 
    * Version: `@vue/cli 4.3.1`
    * https://github.com/vuejs/vue-cli
    * https://cli.vuejs.org/
1. Vuex is required for any state
1. Engine: `Node`
    * Version `"node": "13.11.0"`
1. Built using `Yarn`
    * Version `1.22.4`
1. Typescript is required all code.
    * Version `"typescript": "3.8.3"`
1. Readme.MD with instructrions how to build and run
    * `yarn`
    * `yarn start`
    * It should be run at `http://localhost:8080`
    * In package.json 
        * `"start": "vue-cli-service serve --port 8080"`
        * `"build": "vue-cli-service build"`
1. Directory sturcture as defined:

```
README.md //instructions how to run the solution
babel.config.js 
node_modules // installed packages. approved are mxgraph and any others, but must be in advance
package.json
public
scripts
src // when the project source code is done
tsconfig.json
tsconfig.typedoc.json
vue.config.js
yarn.lock
```

### **Deliverables**

1. Requirements defined above.
1. No iFrames are permitted. Everything needs to be natively done. 
   * **Integrated** and not **wrapped**.
1. A GitLab.com private repo will be provided such that private posts can be made to the protected repository for review.
1. Documentation how to run and manage the app.
1. This exact site running using Vue.js
    * https://jgraph.github.io/mxgraph/javascript/examples/grapheditor/www/index.html
    * **This UX shall be duplicated 100%.**

![image](/uploads/48e3496e2ada855daead2d3b9ec0ae20/image.png)
